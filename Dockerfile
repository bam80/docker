FROM docker.io/manjaroarm/manjaro-aarch64-base

# from PKGBUILD:
#
# pkgname=kwin
# depends=(kscreenlocker xcb-util-cursor plasma-framework kcmutils kwayland-server breeze
#          qt5-script pipewire libqaccessibilityclient lcms2)
# makedepends=(extra-cmake-modules qt5-tools kdoctools krunner)
# optdepends=('qt5-virtualkeyboard: virtual keyboard support for kwin-wayland')
# groups=(plasma)
# source=("https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
#         "https://invent.kde.org/plasma/kwin/-/commit/c4c03e2559f1ecf6f80ae965230bf0a19e17e210.patch")
# 
# build() {
#   cmake -B build -S $pkgname-$pkgver \
#     -DCMAKE_INSTALL_LIBEXECDIR=lib \
#     -DBUILD_TESTING=OFF
# }

RUN pacman --noconfirm --needed -Syu
RUN pacman --noconfirm --needed -S extra-cmake-modules \
				   qt5-tools \
				   krunner \
				   kwin && \
    yes | pacman -Scc		   # clean cache

# get actual KWin sources
RUN pacman -Q kwin | \
	(read pkgname pkgver && pkgver=${pkgver%-*} && \
	 curl -L https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz | tar Jx && \
	 ln -s $pkgname-$pkgver kwin)
#VOLUME kwin

CMD echo Populating the sysroot...

# vim: filetype=dockerfile
